#include <iostream>
#include <string>
#include"Address.h"
#include"Client.h"
#include"Hurtownia_budowlana.h"
#include"Siec_hurtowni_budowlanych.h"

using namespace std;
int main() {

	cout << "Symulacja obslugi klienta w sieci hurtowni budowlanych";

	Siec_hurtowni_budowlanych shb1("XYZ ", "Kreta", 2, "Gdansk");
	Client c1("Janusz", "Kowalski", "Dworcowa", 4, "Gdansk");
	hurtownia_budowlana hb1(shb1, c1, "farby", "ABC", "Dluga", 1, "Rumia");
	
	cout << "Imie klienta pierwszego: " << c1.getImie() << endl;

	cout << "Nazwa sieci hurtowni budowlanej shb1: " << shb1.getNazwa_Sieci_hurtowni_budowlanych() << endl;

	cout << "Pierwsza hurtownia to hurtownia w sieci " << shb1.getNazwa_Sieci_hurtowni_budowlanych() << "z produktami" <<
		hb1.getKategorie_produktow() << "dostarczana dla klienta o nazwisku " << c1.getNazwisko() << endl;

	hb1.przyjecie_towaru();
	hb1.wydanie_towaru();
	hb1.stany_magazynowe();

	system("pause");

	return 0;

}