#pragma once
#include "Siec_hurtowni_budowlanych.h"
#include "Address.h"
#include "Client.h"

Siec_hurtowni_budowlanych::Siec_hurtowni_budowlanych()
{
	cout << "dziala konstruktor klasy Siec_hurtowni_budowlanych bez argumentow\n";
}

Siec_hurtowni_budowlanych::Siec_hurtowni_budowlanych (string naz_S_hurt_bud, string ul, int nr_bud, string mia):
nazwa_Sieci_hurtowni_budowlanych(naz_S_hurt_bud), adres_Siedziby(Address(ul, nr_bud, mia))

{
	cout << "dziala konstruktor klasy Siec_hurtowni_budowlanych z argumentami\n";
}

Siec_hurtowni_budowlanych::Siec_hurtowni_budowlanych(string naz_S_hurt_bud, Address adr_Sied) : nazwa_Sieci_hurtowni_budowlanych
(naz_S_hurt_bud), adres_Siedziby(adr_Sied)

{
	cout << "dziala konstruktor klasy Siec_hurtowni_budowlanych z argumentami \n";

}
void Siec_hurtowni_budowlanych::setNazwa_Sieci_hurtowni_budowlanych(string naz_S_hurt_bud) {
	nazwa_Sieci_hurtowni_budowlanych = naz_S_hurt_bud;}
void Siec_hurtowni_budowlanych::setAdres_Siedziby(Address adr_Sied) { adres_Siedziby = adr_Sied; }

string Siec_hurtowni_budowlanych::getNazwa_Sieci_hurtowni_budowlanych() { return nazwa_Sieci_hurtowni_budowlanych; }
Address Siec_hurtowni_budowlanych::getAdres_Siedziby() { return adres_Siedziby; }