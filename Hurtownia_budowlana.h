#pragma once
#include"Siec_hurtowni_budowlanych.h"
#include"Client.h"
#include"Address.h"
using namespace std;
#include <iostream>

class hurtownia_budowlana : public Siec_hurtowni_budowlanych {
	Client lista_klientow;
	string kategorie_produktow;
	string magazyn;
	Address adres_oddzialu;

public:
	hurtownia_budowlana();
	hurtownia_budowlana(Siec_hurtowni_budowlanych Siec_hurtowni_budowlanych, Client lista_klientow, string
		kategorie_produktow, string magazyn, string ulica, int nr_budynku, string miasto);
	hurtownia_budowlana(string naz_S_hurt_bud, Address adr_Sied, string im, string naz, 
		Address adres_zam, string kategorie_produktow, string magazyn, Address adres_oddzialu);
	 
	void setAdres_oddzialu(Address);
	void setLista_klientow(Client);
	void setKategorie_produktow(string);
	void setMagazyn(string);

	Address getAdres_oddzialu();
	Client getLista_klientow();
	string getKategorie_produktow();
	string getMagazyn();

	double stany_magazynowe();
	double przyjecie_towaru();
	double wydanie_towaru();
		
};