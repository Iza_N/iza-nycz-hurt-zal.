#pragma once
#include <iostream>
#include "Address.h"
using namespace std;

class Client {
	string imie, nazwisko;
	Address adres_zamieszkania;

public:
	Client();
	Client(string imie, string nazwisko, string ulica, int nr_budynku, string miasto);
	Client(string imie, string nazwisko, Address adres_zamieszkania);


	void setImie(string);
	void setNazwisko(string);
	void setAdres_zamieszkania(Address);

	Address getAdres_zamieszkania();
	string getImie();
	string getNazwisko();
};