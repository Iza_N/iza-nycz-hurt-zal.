#pragma once
#include <iostream>
#include "Address.h"

using namespace std;

class Siec_hurtowni_budowlanych {
protected:
	string nazwa_Sieci_hurtowni_budowlanych;
	Address adres_Siedziby;
public: Siec_hurtowni_budowlanych();
	  Siec_hurtowni_budowlanych(string nazwa_Sieci_hurtowni_budowlanych, string ulica, int nr_budynku, string miasto);
	  Siec_hurtowni_budowlanych(string nazwa_Sieci_hurtowni_budowlanych, Address adres_Siedziby);

	  void setNazwa_Sieci_hurtowni_budowlanych(string);
	  void setAdres_Siedziby(Address);

	  string getNazwa_Sieci_hurtowni_budowlanych();
	  Address getAdres_Siedziby();

};
