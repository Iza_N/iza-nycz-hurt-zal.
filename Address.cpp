#pragma once
#include  "Address.h"

Address::Address() : ulica("Pierwsza_ulica") 
{
	cout << "dziala kontruktor klasy adres bez argmentow\n";

}

Address::Address(string ul, int nr_bud, string mia) : ulica(ul), nr_budynku(nr_bud), miasto(mia)
{
	cout << "dziala kontruktor klasy adres z argumentami\n";
}

void Address::setUlica (string ul){	ulica = ul;}
void Address::setNr_budynku(int nr_bud) { nr_budynku = nr_bud; }
void Address::setMiasto(string mia) { miasto = mia; }

string Address::getUlica() {	return ulica;}
int Address::getNr_budynku() { return nr_budynku;}
string Address::getMiasto() {return miasto;}

void Address::displayAddress()
{
	cout << "ulica: " << ulica << "" << "nr_budynku: " << nr_budynku << ",\n" << miasto << endl;
}