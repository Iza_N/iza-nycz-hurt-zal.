#pragma once
# include <iostream>
using namespace std;

class Address {
	string ulica;
	int nr_budynku;
	string miasto;

public:
	Address();
	Address(string ulica, int nr_budynku, string miasto);

	void setUlica(string);
	void setNr_budynku(int);
	void setMiasto(string);

	string getUlica();
	int getNr_budynku();
	string getMiasto();

	void displayAddress();

};
